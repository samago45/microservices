# Documentación del Proyecto: Sistema de Compras y Ventas de Productos

## Introducción
El proyecto consiste en el desarrollo de un sistema de compras y ventas de productos mediante microservicios. El sistema está diseñado para gestionar la creación de productos, clientes, transacciones de compra y venta, así como también para mantener actualizado el stock disponible en un almacén (Producto) compartido.

##  Arquitectura del Sistema

### 1. Componentes Principales
- **Microservicio de Compras de Productos:**
  - API REST para la gestión de productos y compras.
  - Controladores para el manejo de peticiones HTTP.
  - Servicio para la lógica de negocio.
  - Conexión con RabbitMQ para la actualización de stock.
- **Microservicio de Ventas de Productos:**
  - API REST para la gestión de clientes y ventas.
  - Controladores para el manejo de peticiones HTTP.
  - Servicio para la lógica de negocio.
  - Conexión con RabbitMQ para la actualización de stock.
- **Base de Datos:**
  - Esquema compartido para almacenar información de productos, clientes y ventas!

  ![msvc-producto](https://gitlab.com/samago45/microservices/-/raw/main/productodb.png)


  ![msvc-venta](https://gitlab.com/samago45/microservices/-/raw/main/image.png)

 ## 2. Tecnologías Utilizadas
- **Framework:** Spring Boot
- **Cola de Mensajes:** RabbitMQ
- **Base de Datos:** Postgres 
- **Otros:** Eureka (para registro y descubrimiento de servicios), Gateway (para enrutamiento de la API), Config Server (para la gestión centralizada de la configuración).

### 3. Diagrama de Arquitectura (Microservicios)

![arquitectura](https://gitlab.com/samago45/microservices/-/raw/main/microservicio%20-%20arquitectura.png)
 > El diagrama de arquitectura representa de manera visual la estructura y la interacción entre los componentes fundamentales utilizados en el proyecto. Proporciona una visión detallada de cómo los diferentes elementos del sistema se relacionan entre sí y cómo se comunican para lograr los objetivos del proyecto.
Este diagrama muestra claramente la distribución de responsabilidades y la integración entre los microservicios de compras y ventas de productos, la base de datos compartida, RabbitMQ como cola de mensajes,las tecnologías de soporte como Spring Boot y Eureka
  



### 4. Integración de RabbitMQ

![rabbitmq](https://gitlab.com/samago45/microservices/-/raw/main/flujo%20-%20RabbitMQ.png)

> * Producer (Productor): Genera mensajes JSON relacionados con las transacciones de compra o venta de productos.
> * Exchange (Intercambio): Recibe los mensajes del productor y los envía a las colas correspondientes según las reglas de enrutamiento.
> * Queue (Cola): Almacena temporalmente los mensajes en RabbitMQ hasta que sean procesados por los consumidores.
> * Consumer (Consumidor): Recibe los mensajes de las colas y los procesa, actualizando el stock disponible en el almacén compartido según corresponda.

Este flujo de mensajes permite una comunicación asíncrona y distribuida entre los diferentes componentes del sistema, facilitando la escalabilidad y la tolerancia a fallos


`docker run -d --name mi-rabbitmq -p 5672:5672 -p 15672:15672 rabbitmq:management`

>Este comando descarga e inicia RabbitMQ en un contenedor Docker llamado mi-rabbitmq, exponiendo los puertos 5672 y 15672 para acceder al servidor RabbitMQ y a su interfaz de administración web.
Luego, en tus microservicios (mscv-product y mscv-sale), configura los detalles de conexión a RabbitMQ en el microservicio de configuración (mscv-config), utilizando los archivos mscv-product.yml y mscv-sale.yml. Estos archivos deben contener los puertos, intercambios y colas necesarios para la comunicación con RabbitMQ. Esto permitirá que tus microservicios se conecten y envíen mensajes de manera asíncrona, siguiendo el flujo descrito en el diagrama de RabbitMQ.

## 5. Implementación
### Configuración del Proyecto con Maven
- Utiliza un proyecto central de Maven como base común para todos los microservicios.
- Cada microservicio (producto, ventas, config, gateway, eureka) se configura como un módulo dentro del proyecto principal.
- Esto permite una gestión centralizada de las dependencias y la configuración compartida entre los microservicios.

### Conexión al Microservicio de Configuración
- Cada microservicio se conecta al microservicio de configuración para obtener sus propias configuraciones específicas.
- El microservicio de configuración  almacena todas las configuraciones necesarias para cada microservicio en archivos como `application.yml`.
- Al conectar los microservicios al microservicio de configuración, garantizas que todos los microservicios utilicen las mismas configuraciones y que los cambios en la configuración se propaguen de manera eficiente a través de tu sistema.

### Registro de Microservicios con Eureka
- Utiliza Eureka como servidor de registro y descubrimiento de servicios para tu arquitectura basada en microservicios.
- Cada microservicio se registra en el servidor Eureka al iniciarse, lo que permite que otros microservicios y componentes los descubran y se comuniquen con ellos de manera dinámica.

### Gestión de API con API Gateway
- La API Gateway actúa como un punto de entrada centralizado para todas las solicitudes de API.
- Gestiona el enrutamiento de solicitudes entrantes a los microservicios correspondientes, lo que simplifica la arquitectura y facilita la escalabilidad y el mantenimiento.

### Gestión Asíncrona de Actualización de Stock con RabbitMQ
- Utiliza RabbitMQ para gestionar la comunicación asíncrona entre los microservicios para la actualización de stock.
- Cuando se realizan transacciones de compra o venta de productos, los microservicios publican mensajes en RabbitMQ, que luego son consumidos y procesados por otros microservicios para actualizar el stock disponible en el almacén compartido.
- Esto permite una gestión eficiente del stock en tiempo real sin bloquear el flujo principal de ejecución de tus microservicios.



## Sección de API para Pruebas


### Registrar Producto: http://localhost:8080/api/productos

```json
{
    "nombre":"Iphone x",
     "descripcion":"Dispositivo movil",
     "stockDisponible":50,
     "precio": 10000
}
```

### Compra de productos: http://localhost:8080/api/compras
```json
{
  "cantidad": 10,
  "productos": [{
    "idProducto": 1,
    "cantidad": 10
    
  }]

}
```

### Registrar Cliente: http://localhost:8080/api/cliente

```json
{
  "nombre_cliente": "Carlos Gutierres",
  "ruc": "4968817-4"

}
```


### Realizar Venta: http://localhost:8080/api/venta
```json
{
  "cliente": 1,
  "ventas_totales": 1,
  "detallesVentas": [
    {
      "idProducto": 1,
      "nombreProducto": "Producto 1",
      "cantidad": 10,
      "subtotal": 10
    },
    {
      "idProducto": 1,
      "nombreProducto": "Producto 1",
      "cantidad": 5,
      "subtotal": 10
    }
  ]
}
```




