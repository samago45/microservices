package com.microservices.product.repository;

import com.microservices.product.entity.CompraEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ComprasRespository extends JpaRepository<CompraEntity, Long> {
}
