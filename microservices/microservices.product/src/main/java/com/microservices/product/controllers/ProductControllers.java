package com.microservices.product.controllers;

import com.microservices.product.dto.request.ProductoRequest;
import com.microservices.product.services.ProductServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/producto")
public class ProductControllers {
    @Autowired
    private ProductServices productServices;

    @PostMapping()
    public ResponseEntity<?> save(@Validated @RequestBody final ProductoRequest request) {
        try {
            productServices.save(request);
            return new ResponseEntity<>(HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>("Error al guardar el producto " , HttpStatus.BAD_REQUEST);
        }
    }



}
