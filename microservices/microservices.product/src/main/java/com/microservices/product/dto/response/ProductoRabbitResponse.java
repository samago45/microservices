package com.microservices.product.dto.response;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ProductoRabbitResponse {
    private Long productId;
    private String mesage;
    private boolean disponibilidad;
}
