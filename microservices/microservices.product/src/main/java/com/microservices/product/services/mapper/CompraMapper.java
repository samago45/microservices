package com.microservices.product.services.mapper;


import com.microservices.product.dto.request.CompraRequest;
import com.microservices.product.dto.request.ProductosCompraRequest;
import com.microservices.product.entity.CompraEntity;
import com.microservices.product.entity.ProductoEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import java.util.List;
import java.util.stream.Collectors;


@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface CompraMapper {


    @Mapping(target = "cantidad", source = "cantidad")
    CompraEntity toEntity(final CompraRequest compraRequest);

    default List<ProductoEntity> toDetallesCompraEntityList(List<ProductosCompraRequest> productosCompraRequest) {
        return productosCompraRequest.stream().map(this::toDetallesCompraEntity).collect(Collectors.toList());
    }

    @Mapping(target = "id", source = "idProducto")
    @Mapping(target = "stockDisponible", source = "contidad")
    ProductoEntity toDetallesCompraEntity(final ProductosCompraRequest productosCompraRequest);

}
