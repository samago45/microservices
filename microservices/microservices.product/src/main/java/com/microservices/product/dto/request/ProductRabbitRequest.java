package com.microservices.product.dto.request;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ProductRabbitRequest {


    private Long productId;

    private int cantidad;

}
