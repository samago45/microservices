package com.microservices.product.dto.request;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ProductosCompraRequest {

    private Long idProducto;
    private int cantidad;
}
