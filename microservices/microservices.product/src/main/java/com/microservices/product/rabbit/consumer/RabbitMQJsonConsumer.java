package com.microservices.product.rabbit.consumer;

import com.microservices.product.dto.request.ProductRabbitRequest;
import com.microservices.product.services.ProductServices;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;


@Component
public class RabbitMQJsonConsumer {

    private static final Logger log = LoggerFactory.getLogger(RabbitMQJsonConsumer.class);

    @Autowired
    private ProductServices productServices;

    @RabbitListener(queues = {"${spring.rabbitmq.jsonQueueVenta}"})
    public void consumeJsonMessageVenta(List<ProductRabbitRequest> request) {
        try {
            productServices.actualizarStockProductosVentas(request);
            log.info(String.format("Cola recibida de msvc-sales -> %s", request.toString()));
        } catch (Exception e) {
            log.error("Error al actualizar el stock de productos: {}", e.getMessage());
        }
    }

///Recibe el mensaje de compra
    @RabbitListener(queues = {"${spring.rabbitmq.jsonQueueCompra}"})
    public void consumeJsonMessageCompra(List<ProductRabbitRequest> request) {
        try {
            productServices.actualizarStockProductosCompras(request);
            log.info(String.format("Cola recibida de msvc-sales -> %s", request.toString()));
        } catch (Exception e) {
            log.error("Error al actualizar el stock de productos: {}", e.getMessage());
        }
    }



}

