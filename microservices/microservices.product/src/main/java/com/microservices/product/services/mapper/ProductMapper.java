package com.microservices.product.services.mapper;

import com.microservices.product.dto.request.ProductRabbitRequest;
import com.microservices.product.dto.request.ProductoRequest;
import com.microservices.product.entity.ProductoEntity;
import org.mapstruct.*;

import java.util.List;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface ProductMapper {

    @Mappings({
            @Mapping(target = "nombre", source = "nombre"),
            @Mapping(target = "descripcion", source = "descripcion"),
            @Mapping(target = "precio", source = "precio"),
            @Mapping(target = "stockDisponible", source = "stockDisponible"),

    })
    ProductoEntity toEntity(final ProductoRequest request);

    @InheritConfiguration
    ProductoRequest toRequest(final ProductoEntity entity);

    @Named("toEntity")
    @Mapping(target = "id", source = "productId")
    @Mapping(target = "stockDisponible", source = "cantidad")
    ProductoEntity toEntity(ProductRabbitRequest request);

    @IterableMapping(qualifiedByName = "toEntity")
    List<ProductoEntity> toEntityList(List<ProductRabbitRequest> request);


}
