package com.microservices.product.services;

import com.microservices.product.dto.request.CompraRequest;
import com.microservices.product.dto.request.ProductRabbitRequest;
import com.microservices.product.dto.request.ProductosCompraRequest;
import com.microservices.product.entity.CompraEntity;
import com.microservices.product.rabbit.publisher.RabbitMQJsonProducer;
import com.microservices.product.repository.ComprasRespository;
import com.microservices.product.repository.ProductRespository;
import com.microservices.product.services.mapper.CompraMapper;
import jakarta.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CompraServices {

    private static final Logger log = LoggerFactory.getLogger(CompraServices.class);
    @Autowired
    private ProductRespository productRespository;


    @Autowired
    private ComprasRespository comprasRespository;

    @Autowired
    private CompraMapper compraMapper;

    @Autowired
    private RabbitMQJsonProducer rabbitMQJsonProducer;


    @Transactional
    public void postCompra(final CompraRequest compraRequest) {
        log.info("Realizando una nueva compra: {}", compraRequest);

        CompraEntity compra = compraMapper.toEntity(compraRequest);
        CompraEntity compraGuardada = comprasRespository.save(compra);
        log.info("Compra guardada correctamente: {}", compraGuardada);

        List<ProductRabbitRequest> rabbits = new ArrayList<>();

        for (ProductosCompraRequest productosCompraRequest : compraRequest.getProductos()) {
            Long productId = productosCompraRequest.getIdProducto();
            int cantidad = productosCompraRequest.getCantidad();

            // Crear el objeto ProductRabbitRequest y agregarlo a la lista
            ProductRabbitRequest productRabbitRequest = new ProductRabbitRequest(productId, cantidad);
            rabbits.add(productRabbitRequest);
            log.info("Mensaje enviado para el producto con ID {}: Cantidad: {}", productId, cantidad);
        }

        // Envía la solicitud a la cola después de guardar la compra
        rabbitMQJsonProducer.publicarCompra(rabbits);
    }


}
