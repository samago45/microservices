package com.microservices.product.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import reactor.util.annotation.NonNullApi;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class ProductoRequest {

    private String nombre;

    private String descripcion;

    private int stockDisponible;

    private Double precio;
}

