package com.microservices.product.dto.request;

import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CompraRequest {

    private int cantidad ;
    private List<ProductosCompraRequest> productos;
}
