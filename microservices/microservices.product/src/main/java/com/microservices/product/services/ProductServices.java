package com.microservices.product.services;

import com.microservices.product.dto.request.ProductRabbitRequest;
import com.microservices.product.dto.request.ProductoRequest;
import com.microservices.product.entity.ProductoEntity;
import com.microservices.product.repository.ProductRespository;
import com.microservices.product.services.mapper.ProductMapper;
import jakarta.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductServices {
    private static final Logger log = LoggerFactory.getLogger(ProductServices.class);
    @Autowired
    private ProductRespository productRespository;

    @Autowired
    private ProductMapper productMapper;



    @Transactional
    public void save(ProductoRequest request) {
        try {
            ProductoEntity entity = productMapper.toEntity(request);
            productRespository.save(entity);
        } catch (Exception e) {

            log.error("Error al guardar el producto: {}", e.getMessage());

        }
    }

    @Transactional
    public void actualizarStockProductosVentas(List<ProductRabbitRequest> request) {
        request.forEach(this::actualizarStockProductoVenta);
    }


    private void actualizarStockProductoVenta(ProductRabbitRequest product) {
        ProductoEntity entity = productMapper.toEntity(product);
        Optional<ProductoEntity> optionalExistingProduct = productRespository.findById(entity.getId());

        if (!optionalExistingProduct.isPresent()) {
            log.error("Producto no encontrado en la base de datos con ID {}", entity.getId());
            return;
        }

        ProductoEntity existingProduct = optionalExistingProduct.get();
        if (existingProduct.getStockDisponible() < product.getCantidad()) {
            log.error("No hay suficiente stock disponible para el producto con ID {}", entity.getId());
            return;
        }

        int nuevoStock = existingProduct.getStockDisponible() - product.getCantidad();
        existingProduct.setStockDisponible(nuevoStock);

        // Si el nuevo stock es cero, marcar el producto como eliminado
        if (nuevoStock == 0) {
            existingProduct.setEliminado(true);
            log.info("El producto con ID {} se ha marcado como eliminado debido a que su stock ha llegado a cero", entity.getId());
        }

        productRespository.save(existingProduct);
        log.info("Stock actualizado para el producto con ID {}: Nuevo stock: {}", entity.getId(), existingProduct.getStockDisponible());
    }



    @Transactional
    public void actualizarStockProductosCompras(List<ProductRabbitRequest> request) {
        request.forEach(this::actualizarStockProductoCompra);
    }

    private void actualizarStockProductoCompra(ProductRabbitRequest product) {
        ProductoEntity entity = productMapper.toEntity(product);
        Optional<ProductoEntity> optionalExistingProduct = productRespository.findById(entity.getId());

        if (!optionalExistingProduct.isPresent()) {
            log.error("El producto con ID {} no existe en la base de datos", entity.getId());
            return;
        }

        ProductoEntity existingProduct = optionalExistingProduct.get();

        int nuevoStock = existingProduct.getStockDisponible() + product.getCantidad(); // Sumar la cantidad recibida en la compra
        existingProduct.setStockDisponible(nuevoStock);

        productRespository.save(existingProduct);
        log.info("Stock actualizado para el producto con ID {}: Nuevo stock: {}", entity.getId(), existingProduct.getStockDisponible());
    }





}
