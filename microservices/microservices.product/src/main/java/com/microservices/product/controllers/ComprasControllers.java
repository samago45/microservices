package com.microservices.product.controllers;


import com.microservices.product.dto.request.CompraRequest;
import com.microservices.product.entity.CompraEntity;
import com.microservices.product.services.CompraServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/compras")
public class ComprasControllers {

   @Autowired
   private CompraServices compraServices;


    @PostMapping()
    public ResponseEntity<?> save(@Validated @RequestBody final CompraRequest request) {
        try {
            compraServices.postCompra(request);
            return new ResponseEntity<>(HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>("Error al guardar el producto " , HttpStatus.BAD_REQUEST);
        }
    }



}
