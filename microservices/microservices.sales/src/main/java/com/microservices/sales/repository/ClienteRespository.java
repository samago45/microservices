package com.microservices.sales.repository;

import com.microservices.sales.entity.ClienteEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface ClienteRespository  extends JpaRepository<ClienteEntity , Long> {

    Page<ClienteEntity> findByEliminado(Boolean eliminado, Pageable pageable);
}
