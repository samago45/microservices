package com.microservices.sales.services.mappers;

import com.microservices.sales.dto.request.ClienteRequest;
import com.microservices.sales.entity.ClienteEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface ClienteMapper {

    @Mappings({@Mapping(target = "nombre_cliente", source = "nombre_cliente"), @Mapping(target = "ruc", source = "ruc"),


    })
    ClienteEntity toEntity(final ClienteRequest request);


    ClienteRequest toResponse(final ClienteEntity entity);

}
