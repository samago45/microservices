package com.microservices.sales.rabbit.publisher;


import com.microservices.sales.dto.request.ProductRabbitRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@EnableRabbit
public class RabbitMQJsonProducer {
    private static final Logger log = LoggerFactory.getLogger(RabbitMQJsonProducer.class);

    @Value("${spring.rabbitmq.exchange}")
    private String exchange;

    @Value("${spring.ventas.jsonkey.routing.json.key}")
    private String routingJsonKey;

    @Autowired
    private RabbitTemplate rabbitTemplate;



    public void publicarProducto(List<ProductRabbitRequest> request) {
        log.info(String.format("Cola enviada a msvc-product", request.toString()));
        rabbitTemplate.convertAndSend(exchange,routingJsonKey, request);

    }



}
