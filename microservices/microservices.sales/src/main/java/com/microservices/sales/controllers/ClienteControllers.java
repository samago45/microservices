package com.microservices.sales.controllers;
import org.springframework.data.domain.Pageable;
import com.microservices.sales.dto.request.ClienteRequest;
import com.microservices.sales.services.ClienteServices;
import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("api/cliente")
public class ClienteControllers {

    @Autowired
    private ClienteServices clienteServices;

    @PostMapping()
    public ResponseEntity<?> save(@Validated @RequestBody final ClienteRequest request) {
        try {
            clienteServices.save(request);
            return new ResponseEntity<>(HttpStatus.CREATED);
        } catch (ServiceException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping
    public ResponseEntity<Page<ClienteRequest>> findAll(Pageable pageable) {
        return ResponseEntity.ok(clienteServices.findAll(pageable));
    }

    @GetMapping("/{id}")
    public ResponseEntity<ClienteRequest> findById(@PathVariable Long id) {
        return clienteServices.findById(id).map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@PathVariable Long id, @Validated @RequestBody ClienteRequest request) {

        try {
            clienteServices.update(request);
            return ResponseEntity.ok().build();
        } catch (ServiceException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        try {
            clienteServices.delete(id);
            return ResponseEntity.ok().build();
        } catch (ServiceException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
