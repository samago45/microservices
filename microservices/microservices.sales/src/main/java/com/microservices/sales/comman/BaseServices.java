package com.microservices.sales.comman;

import org.springframework.data.domain.Page;

import org.springframework.data.domain.Pageable;
import java.util.Optional;

public interface BaseServices <E , D>{

    Page<E> findAll(Pageable pageable);

    Optional<E> findById(Long id);

    void save(D dto);

    void delete(Long id);

    void update(D dto);
}
