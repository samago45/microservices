package com.microservices.sales.services.mappers;

import com.microservices.sales.dto.request.DetallesVentasRequest;
import com.microservices.sales.dto.request.VentaRequest;
import com.microservices.sales.entity.DetallesVentaEntity;
import com.microservices.sales.entity.VentaEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;

import java.util.List;
import java.util.stream.Collectors;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface VentasMapper {


    @Mappings({@Mapping(target = "cliente.id", source = "cliente"), @Mapping(target = "ventas_totales", source = "ventas_totales"), @Mapping(target = "detalles", source = "detallesVentas")


    })
    VentaEntity toEntity(final VentaRequest request);

    default List<DetallesVentaEntity> toDetallesVentaEntityList(List<DetallesVentasRequest> detallesVentasRequests) {
        return detallesVentasRequests.stream().map(this::toDetallesVentaEntity).collect(Collectors.toList());
    }

    @Mappings({@Mapping(target = "idProducto", source = "idProducto"),
            @Mapping(target = "nombreProducto", source = "nombreProducto"),
            @Mapping(target = "cantidad", source = "cantidad"),
            @Mapping(target = "subtotal", source = "subtotal"),


    })
    DetallesVentaEntity toDetallesVentaEntity(final DetallesVentasRequest detallesVentasRequest);

}
