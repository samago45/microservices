package com.microservices.sales.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DetallesVentasRequest {

    private Long idProducto;

    private String nombreProducto;


    private int cantidad;


    private double subtotal;
}
