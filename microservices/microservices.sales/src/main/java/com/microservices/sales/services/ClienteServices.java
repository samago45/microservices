package com.microservices.sales.services;

import com.microservices.sales.comman.BaseServices;
import com.microservices.sales.dto.request.ClienteRequest;
import com.microservices.sales.entity.ClienteEntity;
import com.microservices.sales.repository.ClienteRespository;
import com.microservices.sales.services.mappers.ClienteMapper;
import jakarta.transaction.Transactional;
import org.hibernate.service.spi.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Pageable;

import java.util.Optional;


@Service
public class ClienteServices  implements BaseServices<ClienteRequest, ClienteRequest> {
    private static final Logger logger = LoggerFactory.getLogger(ClienteServices.class);
    @Autowired
    private ClienteRespository clienteRespository;

    @Autowired
    private ClienteMapper clienteMapper;


    @Transactional()

    @Override
    public Page<ClienteRequest> findAll(Pageable pageable) {
        return clienteRespository.findByEliminado(false, pageable)
                .map(clienteMapper::toResponse);
    }

    @Transactional()
    @Override
    public Optional<ClienteRequest> findById(Long id) {
        return clienteRespository.findById(id).map(clienteMapper::toResponse);
    }

    @Transactional
    @Override
    public void save(ClienteRequest dto) {
        try {
            ClienteEntity entity = clienteMapper.toEntity(dto);
            clienteRespository.save(entity);
        } catch (Exception e) {
            logger.error("Error al guardar el cliente: {}", e.getMessage());
            throw new ServiceException("Error al guardar el cliente", e);
        }
    }

    @Transactional
    @Override
    public void delete(Long id) {
        clienteRespository.deleteById(id);
    }

    @Transactional
    @Override
    public void update(ClienteRequest dto) {
        try {
            ClienteEntity entity = clienteMapper.toEntity(dto);
            clienteRespository.save(entity);
        } catch (Exception e) {
            logger.error("Error al actualizar el cliente: {}", e.getMessage());
            throw new ServiceException("Error al actualizar el cliente", e);
        }
    }
}
