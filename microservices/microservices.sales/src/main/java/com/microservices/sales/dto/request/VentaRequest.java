package com.microservices.sales.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class VentaRequest {

    private int cliente;
    private double ventas_totales;
    private List<DetallesVentasRequest> detallesVentas;


}
