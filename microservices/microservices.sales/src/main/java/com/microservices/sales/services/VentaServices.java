package com.microservices.sales.services;

import com.microservices.sales.entity.DetallesVentaEntity;
import com.microservices.sales.dto.request.ProductRabbitRequest;
import com.microservices.sales.rabbit.publisher.RabbitMQJsonProducer;
import com.microservices.sales.dto.request.VentaRequest;
import com.microservices.sales.entity.VentaEntity;
import com.microservices.sales.repository.VentaRepository;
import com.microservices.sales.services.mappers.VentasMapper;
import jakarta.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class VentaServices {
    private static final Logger logger = LoggerFactory.getLogger(VentaServices.class);

    @Autowired
    private VentasMapper ventasMapper;
    @Autowired
    private VentaRepository ventaRepository;
    @Autowired
    private RabbitMQJsonProducer rabbitMQJsonProducer;

    @Transactional
    public void save(VentaRequest request) {
        try {
            VentaEntity entity = ventasMapper.toEntity(request);
            for (DetallesVentaEntity detalle : entity.getDetalles()) {
                detalle.setVenta(entity);
            }

            entity = ventaRepository.save(entity);

            enviarDetallesVenta(entity.getDetalles());
        } catch (Exception e) {
            logger.error("Error al guardar la venta: {}", e.getMessage());
        }
    }


    private void enviarDetallesVenta(List<DetallesVentaEntity> detallesVentaList) {
        List<ProductRabbitRequest> rabbits = new ArrayList<>();
        for (DetallesVentaEntity detalleVenta : detallesVentaList) {
            Long productId = detalleVenta.getIdProducto();
            int cantidad = detalleVenta.getCantidad();
            ProductRabbitRequest productRabbitRequest = new ProductRabbitRequest(productId, cantidad);
            rabbits.add(productRabbitRequest);
            rabbitMQJsonProducer.publicarProducto(rabbits);
            logger.info("Mensaje enviado para el producto con ID {}: Cantidad: {}", productId, cantidad);
        }
    }


}
