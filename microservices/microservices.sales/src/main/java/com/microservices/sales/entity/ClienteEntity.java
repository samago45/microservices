package com.microservices.sales.entity;

import com.microservices.sales.dto.request.ClienteRequest;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.time.LocalDateTime;


@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "Clientes")
public class ClienteEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    private Long id;
    @Column(name = "nombre_cliente")
    private String nombre_cliente;
    @Column(name = "ruc", unique = true)
    private String ruc;

    @CreationTimestamp
    @Column(name = "creado_en", updatable = false)
    private LocalDateTime creadoEn;
    @UpdateTimestamp
    @Column(name = "actualizado_en")
    private LocalDateTime actualizadoEn;

    private Boolean eliminado = Boolean.FALSE;


    public ClienteEntity(String nombreCliente, ClienteRequest request, String ruc) {
        this.nombre_cliente = nombreCliente;
        this.ruc = ruc;
    }
}
