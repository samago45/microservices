package com.microservices.sales;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicroservicesSalesApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroservicesSalesApplication.class, args);
	}

}
