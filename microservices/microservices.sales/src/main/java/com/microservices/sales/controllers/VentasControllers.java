package com.microservices.sales.controllers;

import com.microservices.sales.dto.request.ClienteRequest;
import com.microservices.sales.dto.request.VentaRequest;
import com.microservices.sales.services.VentaServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/venta")
public class VentasControllers {
    @Autowired
    private VentaServices ventaServices;


    @PostMapping()
    public ResponseEntity<?> save(@Validated @RequestBody final VentaRequest request) {
        try {
            ventaServices.save(request);
            return new ResponseEntity<>(HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>("Error al guardar el producto " , HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
