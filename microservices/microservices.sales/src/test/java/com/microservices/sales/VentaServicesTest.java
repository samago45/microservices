package com.microservices.sales;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;
import com.microservices.sales.dto.request.VentaRequest;
import com.microservices.sales.entity.VentaEntity;
import com.microservices.sales.repository.VentaRepository;
import com.microservices.sales.services.VentaServices;
import com.microservices.sales.services.mappers.VentasMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


public class VentaServicesTest {

    @InjectMocks
    private VentaServices ventaServices;

    @Mock
    private VentasMapper ventasMapper;

    @Mock
    private VentaRepository ventaRepository;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testSave() {
        // Arrange
        VentaRequest request = new VentaRequest(/* Datos de la venta */);
        VentaEntity ventaEntity = new VentaEntity(/* Datos de la venta */);
        when(ventasMapper.toEntity(request)).thenReturn(ventaEntity);
        when(ventaRepository.save(any(VentaEntity.class))).thenReturn(ventaEntity);

        ventaServices.save(request);


    }


}
