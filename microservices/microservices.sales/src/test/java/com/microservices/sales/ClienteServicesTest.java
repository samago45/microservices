package com.microservices.sales;

import com.microservices.sales.dto.request.ClienteRequest;
import com.microservices.sales.entity.ClienteEntity;
import com.microservices.sales.repository.ClienteRespository;
import com.microservices.sales.services.ClienteServices;
import com.microservices.sales.services.mappers.ClienteMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.*;

import org.springframework.data.domain.PageRequest;



import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class ClienteServicesTest {

    @InjectMocks
    private ClienteServices clienteServices;

    @Mock
    private ClienteRespository clienteRespository;

    @Mock
    private ClienteMapper clienteMapper;

    @Test
    public void testFindAll() {
        // Arrange
        Pageable pageable = PageRequest.of(0, 10);
        List<ClienteEntity> entities = new ArrayList<>();
        entities.add(new ClienteEntity(/* datos del cliente */));
        Page<ClienteEntity> page = new PageImpl<>(entities);
        when(clienteRespository.findByEliminado(eq(false),  eq(pageable))).thenReturn(page);

        List<ClienteRequest> expected = new ArrayList<>();
        expected.add(new ClienteRequest(/* datos del cliente */));
        when(clienteMapper.toResponse(any())).thenReturn(expected.get(0));

        // Act
        Page<ClienteRequest> result = clienteServices.findAll(pageable);

        // Assert
        assertEquals(expected, result.getContent());
    }


    @Test
    public void testFindById() {
        // Arrange
        Long id = 1L;
        ClienteEntity entity = new ClienteEntity(/* datos del cliente */);
        when(clienteRespository.findById(eq(id))).thenReturn(Optional.of(entity));

        ClienteRequest expected = new ClienteRequest(/* datos del cliente */);
        when(clienteMapper.toResponse(entity)).thenReturn(expected);

        // Act
        Optional<ClienteRequest> result = clienteServices.findById(id);

        // Assert
        assertTrue(result.isPresent());
        assertEquals(expected, result.get());
    }

    @Test
    public void testSave() {
        // Arrange
        ClienteRequest request = new ClienteRequest("Sebastian" , "200000000");
        ClienteEntity entity = new ClienteEntity(request.getNombre_cliente() , request, request.getRuc());
        when(clienteMapper.toEntity(request)).thenReturn(entity);

        // Act
        clienteServices.save(request);

        // Assert
        verify(clienteRespository).save(entity);
    }

    @Test
    public void testDelete() {
        // Arrange
        Long id = 1L;

        // Act
        clienteServices.delete(id);

        // Assert
        verify(clienteRespository).deleteById(id);
    }

    @Test
    public void testUpdate() {
        // Arrange
        ClienteRequest request = new ClienteRequest(/* datos del cliente */);
        ClienteEntity entity = new ClienteEntity(/* datos del cliente */);
        when(clienteMapper.toEntity(request)).thenReturn(entity);

        // Act
        clienteServices.update(request);

        // Assert
        verify(clienteRespository).save(entity);
    }
}
